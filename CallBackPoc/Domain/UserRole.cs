using Microsoft.AspNetCore.Identity;

namespace CallBackPoc.Domain
{
    public class UserRole : IdentityUserRole<string>
    {
        public Role Role { get; set; }
        public User User { get; set; }
    }
}