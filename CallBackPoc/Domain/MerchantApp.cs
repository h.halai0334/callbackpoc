namespace CallBackPoc.Domain
{
    public class MerchantApp
    {
        public string MerchantId { get; set; }
        public User Merchant { get; set; }
        public string ClientId { get; set; }
        public string Secret { get; set; }
    }
}