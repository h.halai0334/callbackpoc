using Microsoft.AspNetCore.Identity;

namespace CallBackPoc.Domain
{
    public class RoleClaim : IdentityRoleClaim<string>
    {
        public Role Role { get; set; }        
    }
}