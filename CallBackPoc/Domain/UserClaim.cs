using Microsoft.AspNetCore.Identity;

namespace CallBackPoc.Domain
{
    public class UserClaim : IdentityUserClaim<string>
    {
        public User User { get; set; }    
    }
}