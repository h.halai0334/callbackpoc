using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace CallBackPoc.Domain
{
    public class User : IdentityUser
    {
        public User()
        {
            Id = Guid.NewGuid().ToString();
        }
        public string FullName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        /// <summary>
        /// Navigation property for the roles this user belongs to.
        /// </summary>
        public virtual ICollection<UserRole> UserRoles { get; set; }

        /// <summary>
        /// Navigation property for the claims this user possesses.
        /// </summary>
        public virtual ICollection<UserClaim> UserClaims { get; set; }
        /// <summary>
        /// Is User Allowed to Login or Not
        /// </summary>
        public bool IsEnabled { get; set; }

        public IEnumerable<MerchantApp> MerchantApps { get; set; }
    }
}