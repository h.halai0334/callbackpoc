using System;
using CallBackPoc.Context;
using CallBackPoc.Initializer;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CallBackPoc
{
    public class Program
    {
       public static void Main(string[] args)
        {

            IWebHost host = BuildWebHost(args);

            using (IServiceScope scope = host.Services.CreateScope())
            {
                IServiceProvider services = scope.ServiceProvider;
                try
                {
                    scope.ServiceProvider.GetService<ApplicationDbContext>().Database.Migrate();
                    var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
                    DatabaseInitializer.Initialize(context);
                }
                catch (Exception ex)
                {
                    ILogger<Program> logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogCritical("Error Occured When Initializing Database \n" + ex.Message, ex, "DB Name");
                }
            }

            try
            {
                host.Run();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}