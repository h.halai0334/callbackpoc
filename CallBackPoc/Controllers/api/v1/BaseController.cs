﻿using CallBackPoc.Context;
using CallBackPoc.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CallBackPoc.Controllers.api.v1
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class BaseController : Controller
    {
        private UserManager<User> _userManager;
        private ApplicationDbContext _applicationDbContext;
        protected UserManager<User> UserManager => _userManager ??= (UserManager<User>) HttpContext.RequestServices.GetService(
            typeof(UserManager<User>));
        
        protected ApplicationDbContext ApplicationDbContext => _applicationDbContext ??= (ApplicationDbContext) HttpContext.RequestServices.GetService(
            typeof(ApplicationDbContext));

    }
}