﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CallBackPoc.Attributes;
using CallBackPoc.Constants;
using CallBackPoc.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CallBackPoc.Controllers.api.v1
{
    public class CallBackController : BaseController
    {
        [Authorize(Roles = RoleNames.User)]
        [HttpPost("byUser")]
        public IActionResult CallBackByUser(object jsonBody)
        {
            // Add your own logic
            var model = new Recovery()
            {
                Carrier = "Carrier",
                Destination = "Destination",
                Email = "test@yopmail.com",
                Id = 10,
                ContactName = "Test name",
                CustomerName = "Test Name",
                PhoneNumber = "+123456789",
                RecoveryType = "Static",
                ScheduleDateTime = DateTime.UtcNow.ToString("G"),
                RequestFile = ImageUtil.ImageFile,
                Assets = new List<Asset>()
                {
                    new Asset()
                    {
                        Manufacturer = "Test Manufacture",
                        Quantity = 10,
                        AdditionalNotes = "None",
                        AssetType = "Static",
                        EstimatedContainer = 10,
                        EstimatedPieces = 10,
                        ModelNumber = "123456"
                    },
                    new Asset()
                    {
                        Manufacturer = "Test Manufacture 1",
                        Quantity = 15,
                        AdditionalNotes = "None 2",
                        AssetType = "Static 3 ",
                        EstimatedContainer = 100,
                        EstimatedPieces = 102,
                        ModelNumber = "123456666"
                    },
                },
                LogisticServices = new List<LogisticService>()
                {
                    new LogisticService()
                    {
                        Id = 10,
                        Recovery = "None",
                        Service = "None"
                    },
                    new LogisticService()
                    {
                        Id = 11,
                        Recovery = "None1",
                        Service = "None2"
                    },
                }
            };
            return Ok(model);
        }
        
        [HmacAuthentication]
        [HttpPost("byApp")]
        public async Task<IActionResult> CallBackByApp()
        {
            HttpContext.Request.EnableBuffering();
            var reader = new StreamReader(HttpContext.Request.Body);
            reader.BaseStream.Seek(0, SeekOrigin.Begin);
            var content = await reader.ReadToEndAsync();
            // Add your own logic
            return Ok();
        }
    }
}