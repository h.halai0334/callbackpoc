﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Primitives;
using CallBackPoc.Constants;
using CallBackPoc.Context;
using CallBackPoc.Domain;
using CallBackPoc.Extensions;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using OpenIddict.Abstractions;
using OpenIddict.Server;

namespace CallBackPoc.Controllers
{
    public class AuthorizationController : Controller
    {
        private readonly IOptions<IdentityOptions> _identityOptions;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly ApplicationDbContext _context;

        public AuthorizationController(
            IOptions<IdentityOptions> identityOptions,
            SignInManager<User> signInManager,
            UserManager<User> userManager, ApplicationDbContext context)
        {
            _identityOptions = identityOptions;
            _signInManager = signInManager;
            _userManager = userManager;
            _context = context;
        }


        public class OpenIdRoleConnectRequest : OpenIdConnectRequest
        {
        }


        [HttpPost("~/connect/token")]
        [Produces("application/json")]
        public async Task<IActionResult> Exchange(OpenIdRoleConnectRequest requestObject)
        {
            if (requestObject.IsPasswordGrantType())
            {
                Expression<Func<User, bool>> query = p =>
                    p.PhoneNumber.Equals(requestObject.Username) || p.Email.Equals(requestObject.Username);

                var user = await _context.Users.GetByAsync(query);
                if (user == null)
                {
                    return BadRequest(new {error = "Please check that whether your credentials are correct or not"});
                }

                // Ensure the user is enabled.
                if (!user.IsEnabled)
                {
                    return BadRequest(new {error = "The specified user account has been blocked"});
                }


                // Validate the username/password parameters and ensure the account is not locked out.
                var result = await _signInManager.CheckPasswordSignInAsync(user, requestObject.Password, true);

                // Ensure the user is not already locked out.
                if (result.IsLockedOut)
                {
                    return BadRequest(new {error = "The specified user account has been suspended"});
                }


                // Ensure the user is allowed to sign in.
                if (result.IsNotAllowed)
                {
                    return BadRequest(new {error = "The specified user is not allowed to sign in"});
                }

                if (!result.Succeeded)
                {
                    return BadRequest(new {error = "Please check that your email and password is correct"});
                }


                // Create a new authentication ticket.
                var ticket = CreateTicket(requestObject, user);

                return SignIn(ticket.Principal, ticket.Properties, ticket.AuthenticationScheme);
            }
            else if (requestObject.IsRefreshTokenGrantType())
            {
                // Retrieve the claims principal stored in the refresh token.
                var info = await HttpContext.AuthenticateAsync(OpenIddictServerDefaults.AuthenticationScheme);

                // Retrieve the user profile corresponding to the refresh token.
                // Note: if you want to automatically invalidate the refresh token
                // when the user password/roles change, use the following line instead:
                // var user = _signInManager.ValidateSecurityStampAsync(info.Principal);
                var userId = info.Principal.FindFirstValue("userId");
                var user = await _context.Users.GetByReadOnlyAsync(p => p.Id == userId);
                if (user == null)
                {
                    return BadRequest(new {error = "The refresh token is no longer valid"});
                }

                // Ensure the user is enabled.
                if (!user.IsEnabled)
                {
                    return BadRequest(new {error = "The specified user account has been blocked"});
                }

                // Ensure the user is still allowed to sign in.
                if (!await _signInManager.CanSignInAsync(user))
                {
                    return BadRequest(new {error = "The user is no longer allowed to sign in"});
                }

                // Create a new authentication ticket, but reuse the properties stored
                // in the refresh token, including the scopes originally granted.
                var ticket = CreateTicket(requestObject, user);

                return SignIn(ticket.Principal, ticket.Properties, ticket.AuthenticationScheme);
            }

            return BadRequest(new {error = "The specified grant type is not supported"});
        }

        private AuthenticationTicket CreateTicket(OpenIdConnectRequest request, User user)
        {
            // Create a new ClaimsPrincipal containing the claims that
            // will be used to create an id_token, a token or a code.
            //            var principal = _signInManager.CreateUserPrincipalAsync(user).Result;
            //            var claims = _userManager.GetClaimsAsync(user).Result;
            var roles = _userManager.GetRolesAsync(user).Result;
            var claims = new List<Claim>();
            {
            }
            ;
            claims.AddRange(roles.Select(p => new Claim(ClaimTypes.Role, p)).ToArray());
            var userClaims = _context.UserClaims.Where(p => p.UserId == user.Id).ToList();

            var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, "Basic"));
            // Create a new authentication ticket holding the user identity.
            var ticket = new AuthenticationTicket(principal, new AuthenticationProperties(),
                OpenIddictServerDefaults.AuthenticationScheme);
            ticket.SetAccessTokenLifetime(TimeSpan.FromMinutes(60));
            ticket.SetIdentityTokenLifetime(TimeSpan.FromMinutes(60));
            ticket.SetRefreshTokenLifetime(TimeSpan.FromDays(1));
            //if (!request.IsRefreshTokenGrantType())
            //{
            // Set the list of scopes granted to the client application.
            // Note: the offline_access scope must be granted
            // to allow OpenIddict to return a refresh token.
            ticket.SetScopes(new[]
            {
                OpenIdConnectConstants.Scopes.OpenId,
                OpenIdConnectConstants.Scopes.Email,
                OpenIdConnectConstants.Scopes.Phone,
                OpenIdConnectConstants.Scopes.Profile,
                OpenIdConnectConstants.Scopes.OfflineAccess,
                OpenIddictConstants.Scopes.Roles
            }.Intersect(request.GetScopes()));
            //}


            //ticket.SetResources("quickapp-api");

            // Note: by default, claims are NOT automatically included in the access and identity tokens.
            // To allow OpenIddict to serialize them, you must attach them a destination, that specifies
            // whether they should be included in access tokens, in identity tokens or in both.
            foreach (var claim in ticket.Principal.Claims)
            {
                // Never include the security stamp in the access and identity tokens, as it's a secret value.
                if (claim.Type == _identityOptions.Value.ClaimsIdentity.SecurityStampClaimType)
                    continue;


                var destinations = new List<string> {OpenIdConnectConstants.Destinations.AccessToken};

                // Only add the iterated claim to the id_token if the corresponding scope was granted to the client application.
                // The other claims will only be added to the access_token, which is encrypted when using the default format.
                if ((claim.Type == OpenIdConnectConstants.Claims.Subject &&
                     ticket.HasScope(OpenIdConnectConstants.Scopes.OpenId)) ||
                    (claim.Type == OpenIdConnectConstants.Claims.Name &&
                     ticket.HasScope(OpenIdConnectConstants.Scopes.Profile)) ||
                    (claim.Type == OpenIdConnectConstants.Claims.Role &&
                     ticket.HasScope(OpenIddictConstants.Claims.Roles)))
                {
                    destinations.Add(OpenIdConnectConstants.Destinations.IdentityToken);
                }

                claim.SetDestinations(destinations);
            }

            var identity = principal.Identity as ClaimsIdentity;
            if (identity == null)
            {
                throw new Exception("Error: Principle is Null");
            }
            identity.AddClaim(OpenIdConnectConstants.Claims.Subject, user.Email, OpenIdConnectConstants.Destinations.IdentityToken);
            identity.AddClaim(CustomClaimTypes.UserId, user.Id, OpenIdConnectConstants.Destinations.IdentityToken);
            identity.AddClaim(CustomClaimTypes.UserId, user.Id, OpenIdConnectConstants.Destinations.AccessToken);

            return ticket;
        }
    }
}