﻿namespace CallBackPoc.Models
{
    public class LogisticService
    {
        public int Id { get; set; }
        public string Recovery { get; set; }
        public string Service { get; set; }
    }
}