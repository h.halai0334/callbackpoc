﻿using System.Collections.Generic;

namespace CallBackPoc.Models
{
    public class Recovery
    {
        public int Id { get; set; }
        public string RecoveryType { get; set; }
        public string Destination { get; set; }
        public string ScheduleDateTime { get; set; }
        public string Carrier { get; set; }
        public string CustomerName { get; set; }
        public string ContactName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string RequestFile { get; set; }
        public List<Asset> Assets { get; set; }
        public List<LogisticService> LogisticServices { get; set; }
    }
}