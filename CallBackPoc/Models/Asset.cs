﻿namespace CallBackPoc.Models
{
    public class Asset
    {
        public int EstimatedContainer { get; set; }
        public int EstimatedPieces { get; set; }
        public string AdditionalNotes { get; set; }
        public string AssetType { get; set; }
        public string Manufacturer { get; set; }
        public string ModelNumber { get; set; }
        public int Quantity { get; set; }
    }
}