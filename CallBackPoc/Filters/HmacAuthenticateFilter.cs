﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CallBackPoc.Attributes;
using CallBackPoc.Constants;
using CallBackPoc.Context;
using CallBackPoc.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;

namespace CallBackPoc.Filters
{
    public class HmacAuthenticateFilter : IAsyncAuthorizationFilter
    {
        private readonly UInt64 requestMaxAgeInSeconds = 300; //Means 5 min
        private readonly ApplicationDbContext _dbContext;
        private readonly IMemoryCache _cache;

        public HmacAuthenticateFilter(ApplicationDbContext dbContext, IMemoryCache cache)
        {
            _dbContext = dbContext;
            _cache = cache;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var descriptor = (context.ActionDescriptor as ControllerActionDescriptor);
            var attrib = descriptor?.MethodInfo.GetCustomAttributes(typeof(HmacAuthenticationAttribute), false)
                .FirstOrDefault();
            if (attrib == null)
            {
                return;
            }

            HttpRequest req = context.HttpContext.Request;
            if (req.Headers["Authorization"] != StringValues.Empty) {
                var rawAuthzHeader = req.Headers["Authorization"].ToString();
                var authorizationHeaderArray = GetAuthorizationHeaderValues(rawAuthzHeader);
                if (authorizationHeaderArray != null)
                {
                    var appId = authorizationHeaderArray[0];
                    if (appId.StartsWith("hmacauth"))
                    {
                        appId = appId.Replace("hmacauth", "").Trim();
                    }
                    else
                    {
                        context.Result = new UnauthorizedObjectResult(
                            new
                            {
                                success = false,
                                message = "Invalid Request"
                            });
                        return;
                    }
                    var incomingBase64Signature = authorizationHeaderArray[1];
                    var nonce = authorizationHeaderArray[2];
                    var requestTimeStamp = authorizationHeaderArray[3];
                    var isValid = await IsValidRequest(req, appId, incomingBase64Signature, nonce, requestTimeStamp);
                    if (isValid)
                    {
                        var claim = new Claim(CustomClaimTypes.AppId, appId);

                        var currentPrincipal = new GenericPrincipal(new ClaimsIdentity(new List<Claim>()
                        {
                            claim
                        }), null);
                        context.HttpContext.User = currentPrincipal;
                    }
                    else
                    {
                        context.Result = new UnauthorizedObjectResult(
                            new
                            {
                                success = false,
                                message = "Invalid Request"
                            });
                    }
                }
                else
                {
                    context.Result = new UnauthorizedObjectResult(
                        new
                        {
                            success = false,
                            message = "Invalid Request"
                        });
                }
            }
            else
            {
                context.Result = new UnauthorizedObjectResult(
                    new
                    {
                        success = false,
                        message = "Invalid Request"
                    });
            }
        }


        private async Task<bool> IsValidRequest(HttpRequest req, string appId, string incomingBase64Signature,
            string nonce, string requestTimeStamp)
        {
            string requestContentBase64String = "";
            string requestUri = HttpUtility.UrlEncode(req.Path.ToString().ToLower());
            string requestHttpMethod = req.Method;
            var merchantApp = await _dbContext.MerchantApps.GetByReadOnlyAsync(p => p.ClientId == appId);
            if (merchantApp == null)
            {
                return false;
            }
            var sharedKey = merchantApp.Secret;
            if (IsReplayRequest(nonce, requestTimeStamp))
            {
                return false;
            }


            (byte[] hash,string content) = await ComputeHash(req);
            if (hash != null)
            {
                requestContentBase64String = Convert.ToBase64String(hash);
            }

            string data =
                $"{appId}{requestHttpMethod}{requestUri}{requestTimeStamp}{nonce}{requestContentBase64String}";
            var secretKeyBytes = Encoding.ASCII.GetBytes(sharedKey);
            byte[] signature = Encoding.UTF8.GetBytes(data);
            using HMACSHA256 hmac = new HMACSHA256(secretKeyBytes);
            byte[] signatureBytes = hmac.ComputeHash(signature);
            var generatedBaseSignature = Convert.ToBase64String(signatureBytes);
            return incomingBase64Signature == generatedBaseSignature;
        }

        private bool IsReplayRequest(string nonce, string requestTimeStamp)
        {
            object data = null;
            if (_cache.TryGetValue(nonce, out data))
            {
                return true;
            }

            DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan currentTs = DateTime.UtcNow - epochStart;
            var serverTotalSeconds = Convert.ToUInt64(currentTs.TotalSeconds);
            var requestTotalSeconds = Convert.ToUInt64(requestTimeStamp);
            // if ((serverTotalSeconds - requestTotalSeconds) > requestMaxAgeInSeconds)
            // {
            //     return true;
            // }

            _cache.Set(nonce, new { }, DateTimeOffset.Now.AddMinutes(5));
            return false;
        }

        private static async Task<(byte[],string)> ComputeHash(HttpRequest request)
        {
            using MD5 md5 = MD5.Create();
            byte[] hash = null;
            request.EnableBuffering();
            var reader = new StreamReader(request.Body);
            reader.BaseStream.Seek(0, SeekOrigin.Begin);
            var content = await reader.ReadToEndAsync();
            if (content.Length != 0)
            {
                hash = md5.ComputeHash(Encoding.ASCII.GetBytes(content));
            }

            return (hash, content);
        }

        private string[] GetAuthorizationHeaderValues(string rawAuthzHeader)
        {
            var credArray = rawAuthzHeader.Split(':');
            if (credArray.Length == 4)
            {
                return credArray;
            }
            else
            {
                return null;
            }
        }
    }
}