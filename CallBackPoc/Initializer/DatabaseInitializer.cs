using System;
using System.Collections.Generic;
using System.Linq;
using CallBackPoc.Constants;
using CallBackPoc.Context;
using CallBackPoc.Domain;
using Microsoft.AspNetCore.Identity;

namespace CallBackPoc.Initializer
{
    public class DatabaseInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {
            DatabaseInitializer initializer = new DatabaseInitializer();
            initializer.SeedEverything(context);
        }

        private void SeedEverything(ApplicationDbContext context)
        {
            SeedRoles(context);
            SeedUsers(context);
        }

        

        private void SeedRoles(ApplicationDbContext context)
        {
            if (context.Roles.Any())
            {
                return;
            }

            var roles = new []{ RoleNames.User};
            foreach (var role in roles)
            {
                context.Roles.Add(new Role()
                {
                    Name = role,
                    NormalizedName = role.ToUpper()
                });                
            }
            context.SaveChanges();
        }

        private void SeedUsers(ApplicationDbContext context)
        {
            if (context.Users.Any())
            {
                return;
            }

            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
            string adminRoleId = context.Roles.First(p => p.Name == RoleNames.User).Id;
            var admin = new User()
            {
                PhoneNumber = "+123456789",
                Email = "test@yopmail.com",
                IsEnabled = true,
                NormalizedEmail = "TEST@YOPMAIL.COM",
                FullName = "User",
                SecurityStamp = Guid.NewGuid().ToString("D"),
                MerchantApps = new []
                {
                    new MerchantApp()
                    {
                        Secret = Guid.NewGuid().ToString("N") + "." + Guid.NewGuid().ToString("N"),
                        ClientId = Guid.NewGuid().ToString("N")
                    }, 
                }
            };
            admin.UserRoles = new List<UserRole>()
            {
                new UserRole()
                {
                    RoleId = adminRoleId,
                    UserId = admin.Id
                }
            };
            admin.PasswordHash = passwordHasher.HashPassword(admin, "Lahore123@");
            context.Users.Add(admin);
            context.SaveChanges();
        }
    }
}