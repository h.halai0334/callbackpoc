﻿using CallBackPoc.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CallBackPoc.Configuration
{
    public class MerchantAppConfiguration : IEntityTypeConfiguration<MerchantApp>
    {
        public void Configure(EntityTypeBuilder<MerchantApp> builder)
        {
            builder.HasKey(p => p.ClientId);
            builder.HasOne(p => p.Merchant)
                .WithMany(p => p.MerchantApps)
                .HasForeignKey(p => p.MerchantId);
        }
    }
}