using CallBackPoc.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CallBackPoc.Configuration
{

    public class RoleClaimConfiguration : IEntityTypeConfiguration<RoleClaim>
    {
        public void Configure(EntityTypeBuilder<RoleClaim> builder)
        {
            builder.HasOne(p => p.Role)
                .WithMany(p => p.RoleClaims)
                .HasForeignKey(p => p.RoleId);
        }
    }

}