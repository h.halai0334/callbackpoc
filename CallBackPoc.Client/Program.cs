﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CallBackPoc.Client
{
    class AuthResponse
    {
        public string access_token { get; set; }
    }
    class Program
    {
        private static AuthResponse _authResponse = new AuthResponse();
        private static string apiBaseAddress = "https://localhost:5001/";
        private static bool _isRunning = true;
        static async Task Main(string[] args)
        {
            while (_isRunning)
            {
                PrintOptions();
                var result = ReadOptions();
                switch (result)
                {
                    case 1:
                        await Authenticate();
                        break;
                    case 2:
                        await SendPostResponse();
                        break;

                    case 3:
                        await RunHmacAsync();
                        break;
                    case 4:
                        _isRunning = false;
                        break;
                }
            }
        }

        static void PrintOptions()
        {
            Console.WriteLine("1. Authenticate");
            Console.WriteLine("2. Send JWT Request");
            Console.WriteLine("3. Send HMAC Request");
            Console.WriteLine("4. Exit");
        }

        static int ReadOptions()
        {
            var input = Console.ReadLine();
            int.TryParse(input, out var opt);
            return opt;
        }


        //Send Notification Sending
        static async Task SendPostResponse()
        {
            Console.WriteLine("Calling the CallBack By User back-end API");
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",_authResponse.access_token);
            HttpResponseMessage response = await client.PostAsJsonAsync(apiBaseAddress + "api/v1/CallBack/ByUser", new
            {
                phoneNumber = "+123456789",
                refNo = "hello123",
            });
            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseString);
                Console.WriteLine("HTTP Status: {0}, Reason {1}. Press ENTER to exit", response.StatusCode, response.ReasonPhrase);
            }
            else
            {
                Console.WriteLine("Failed to call the API. HTTP Status: {0}, Reason {1}", response.StatusCode, response.ReasonPhrase);
            }
        }
        static async Task Authenticate()
        {
            Console.WriteLine("Calling the Authentication back-end API");
            HttpClient client = new HttpClient();
            var body = new List<KeyValuePair<string, string>>();
            body.Add(new KeyValuePair<string, string>("username", "test@yopmail.com"));
            body.Add(new KeyValuePair<string, string>("password", "Lahore123@"));
            body.Add(new KeyValuePair<string, string>("grant_type", "password"));
            body.Add(new KeyValuePair<string, string>("granttype", "password"));
            body.Add(new KeyValuePair<string, string>("scope", "openid email profile offline_access roles"));
            var req = new HttpRequestMessage(HttpMethod.Post, apiBaseAddress + "connect/token") { Content = new FormUrlEncodedContent(body) };
            var res = await client.SendAsync(req);
            string responseString = await res.Content.ReadAsStringAsync();
            if (res.IsSuccessStatusCode)
            {
                Console.WriteLine("Authentication Success");
                Console.WriteLine(responseString);
                _authResponse = JsonConvert.DeserializeObject<AuthResponse>(responseString);
            }
            else
            {
                Console.WriteLine("Authentication Not Success");
                Console.WriteLine(responseString);
            }
            
        }
        static async Task RunHmacAsync()
        {
            Console.WriteLine("Calling the CallBack By App back-end API");
            //Need to change the port number
            //provide the port number where your api is running
            HmacDelegatingHandler customDelegatingHandler = new HmacDelegatingHandler();
            HttpClient client = HttpClientFactory.Create(customDelegatingHandler);
            HttpResponseMessage response = await client.PostAsJsonAsync(apiBaseAddress + "api/v1/CallBack/ByApp", new
            {
                phoneNumber = "+123456789",
                refNo = "hello123",
            });
            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync();
                Console.WriteLine(responseString);
                Console.WriteLine("HTTP Status: {0}, Reason {1}. Press ENTER to exit", response.StatusCode, response.ReasonPhrase);
            }
            else
            {
                Console.WriteLine("Failed to call the API. HTTP Status: {0}, Reason {1}", response.StatusCode, response.ReasonPhrase);
            }
        }
    }
}